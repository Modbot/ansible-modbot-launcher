Modbot Launcher Ansible Role
=========

This role downloads and installs the Modbot Launcher.

Installs
------------
- Modbot Launcher

Requirements
------------
N/A

Role Variables
--------------

- `modbot_launcher_url`: URL of the Modbot Launcher download.
- `modbot_launcher_temp_dir`: Location used for temporary Modbot Launcher archive download.


Dependencies
------------
N/A